Configuration Provider for HashiCorp Vault 
==========================================
[![Pipeline Status](https://gitlab.com/axual/public/vault-config-provider/badges/master/pipeline.svg)](https://gitlab.com/axual/public/vault-config-provider/commits/master) 
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=axual-public-vault-config-provider&metric=coverage&token=2ad0d9c7f31913b0c23317f8196f05d76a4543d8)](https://sonarcloud.io/dashboard?id=axual-public-vault-config-provider)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=axual-public-vault-config-provider&metric=sqale_rating&token=2ad0d9c7f31913b0c23317f8196f05d76a4543d8)](https://sonarcloud.io/dashboard?id=axual-public-vault-config-provider)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=axual-public-vault-config-provider&metric=alert_status&token=2ad0d9c7f31913b0c23317f8196f05d76a4543d8)](https://sonarcloud.io/dashboard?id=axual-public-vault-config-provider)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

An implementation of the Kafka ConfigProvider interface to retrieve data from HashiCorp Vault

[[_TOC_]]

How to use
----------
The ConfigProvider is a means for Kafka Applications to retrieve secret configuration options and provide them to the implementations without showing the contents in the output. 
The Configuration Provider for HashiCorp Vault can connect to a HashiCorp Vault installation and retrieve one or more keys and secrets stored.

```java
import org.apache.kafka.common.config.ConfigData;

import java.util.HashMap;
import java.util.Map;

import io.axual.utilities.config.providers.VaultConfigProvider;
import io.axual.utilities.config.providers.VaultHelperConfig;

public class Example {
    public static void main(String[] args) {
        final String ROLE_ID = "myrole";
        final String SECRET_ID = "mysecret";
        final String ADDRESS = "http://vault:8200";
        final String SECRET_PATH = "producer/my-app/config";

        Map<String, Object> vaultConfiguration = new HashMap<>();

        // Set address property
        vaultConfiguration.put(VaultHelperConfig.VAULT_ADDRESS_CONFIG, ADDRESS);

        // Set credentials
        vaultConfiguration.put(VaultHelperConfig.VAULT_AUTH_METHOD_CONFIG, VaultHelperConfig.VAULT_AUTH_METHOD_APPROLE); // set auth.method
        vaultConfiguration.put(VaultHelperConfig.VAULT_CREDENTIAL_APPROLE_ROLE_ID_CONFIG, ROLE_ID); // set approle.role.id
        vaultConfiguration.put(VaultHelperConfig.VAULT_CREDENTIAL_APPROLE_SECRET_ID_CONFIG, SECRET_ID); // set approle.secret.id

        VaultConfigProvider configProvider = new VaultConfigProvider();
        configProvider.configure(vaultConfiguration);

        ConfigData configData = configProvider.get(SECRET_PATH);
        Map<String, String> contents = configData.data();

        for (String key : contents.keySet()) {
            System.out.println("Key: " + key + "  Value : " + contents.get(key));
        }
    }
}

```

Configuration Options
---------------------
The configuration definition can be found in the class ```io.axual.utilities.config.providers.VaultHelperConfig```

| Config                          |  Type     | Required |Default           | Description   |
|:--------------------------------|:---------:|:--------:|:----------------:|:--------------|
| ***address***                   | STRING    | Yes      |                  | The URL where HashiCorp Vault can be reached  |
| ***timeout.open***              | INTEGER   | No       | ```30```         | The number of seconds to wait before giving up on establishing an HTTP(S) connection to the Vault server  |
| ***timeout.read***              | INTEGER   | No       | ```30```         | After an HTTP(S) connection has already been established, this is the number of seconds to wait for all data to finish downloading  |
| ***namespace***                 | STRING    | No       | ```null```       | Sets a global namespace to the Vault server instance |
| ***global.engine.version***     | INTEGER   | No       | ```2```          | Sets the KV Secrets Engine version of the Vault server instance |
| ***prefix.path.depth***         | INTEGER   | No       | ```1```          | Set the "path depth" of the prefix path |
| ***prefix.path***               | STRING    | No       | ```null```       | Set the "path depth" of the prefix path by specifying the path. /a/b/c would result in a prefix path depth of 3 |
| ***ssl.verify***                | BOOLEAN   | No       | ```true```       | Whether or not HTTPS connections to the Vault server should verify that a valid SSL certificate is being used |
| ***ssl.truststore.location***   | STRING    | No       | ```null```       | The path to a JKS keystore file, containing the Vault TLS Certificate or Certificate Authorities |
| ***ssl.truststore.password***   | PASSWORD  | No       | ```null```       | The password to access the JKS keystore file containing the Vault TLS Certificate or Certificate Authorities |
| ***auth.method***               | STRING    | No       | ```"APPROLE"```  | The authentication method to use. Valid values are APPROLE |
| ***approle.path***              | STRING    | No       | ```"approle"```  | The path on which the authentication is performed, following the "/v1/auth/" prefix (e.g. "approle") |
| ***approle.role.id***           | STRING    | No       | ```null```       | The vault role id to use for communicating with Vault |
| ***approle.secret.id***         | PASSWORD  | No       | ```null```       | The vault secret id to use for communicating with Vault |
| ***test.path***                 | STRING    | No       | ```null```       | The path of the data to retrieve from Vault during configuration |

Known Limitations
-----------------
* Does not implement the Kafka ConfigProvider subscribe methods
* AppRole authentication only
* No PKI support
* Java 11 or newer JVM needed

License
-------
Configuration Provider for HashiCorp Vault is licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt).