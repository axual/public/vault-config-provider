# Changelog
All notable changes to this project will be documented in this file.

## [1.1.0] 2024-11-21
* Added Integration Test with Kafka TestContainer
* Update to Java 11 JAR
* Updating dependencies and plugin versions
  Dependencies for Kafka
  * Kafka Client 2.3.1 -> 3.9.0

  Test dependencies:
  * TestContainers 1.14.3 -> 1.20.3
  * Junit Jupiter 5.6.2 -> 5.11.3
  * Mockito 3.3.3 -> 5.14.2
  * Common Lang3 3.9 -> 3.17.0
  * Guava 29.0-jre -> 33.3.1-jre
  * Slf4j 1.7.30 -> 1.7.36

  Plugins:
  * jacoco-maven-plugin 0.8.5 -> 0.8.12
  * nexus-staging-maven-plugin 1.6.7 -> 1.7.0
  * license-maven-plugin 1.9 -> 1.20
  * owasp-dependency-check-plugin 5.3.2 -> 11.1.0
  * maven-assembly-plugin 3.2.0 -> 3.7.1
  * maven-clean-plugin 3.1.0 -> 3.4.0
  * maven-compiler-plugin 3.8.1 -> 3.13.0
  * maven-dependency-plugin 3.1.2 -> 3.8.1
  * maven-jar-plugin 3.2.0 -> 3.4.2
  * maven-javadoc-plugin 3.2.0 -> 3.11.1
  * maven-source-plugin 3.2.1 -> 3.3.1
  * maven-assembly-plugin 3.2.0 -> 3.7.1
  * maven-resources-plugin 3.1.0 -> 3.3.1
  * maven-failsafe-plugin 3.0.0-M7 -> 3.5.2
  * maven-surefire-plugin 3.0.0-M7 -> 3.5.2
  * maven-deploy-plugin 2.8.2 -> 3.1.3
  * maven-install-plugin 2.5.2 -> 3.1.3


## [1.0.0] - 2020-07-08
* Initial release of Configuration Provider for Hashicorp Vault

[master]:https://gitlab.com/axual/public/vault-config-provider/-/compare/1.1.0...master
[1.1.0]:https://gitlab.com/axual/public/vault-config-provider/-/compare/1.0.0...1.1.0
[1.0.0]: https://gitlab.com/axual/public/vault-config-provider/-/tree/1.0.0
