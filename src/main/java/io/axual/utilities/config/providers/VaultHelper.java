package io.axual.utilities.config.providers;

/*-
 * ========================LICENSE_START=================================
 * Configuration Provider for HashiCorp Vault
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import com.bettercloud.vault.Vault;
import com.bettercloud.vault.VaultConfig;
import com.bettercloud.vault.VaultException;
import com.bettercloud.vault.json.WriterConfig;
import com.bettercloud.vault.response.AuthResponse;
import com.bettercloud.vault.response.LogicalResponse;
import com.bettercloud.vault.response.VaultResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.axual.utilities.config.providers.exceptions.VaultConfigurationProviderException;

public class VaultHelper {
    public final Logger log;
    private final VaultHelperConfig configProviderConfig;

    /**
     * A helper class to access HashiCorp Vault
     *
     * @param configProviderConfig VaultConfigProvider configuration object containing the connection settings
     * @param logger               A Logger object who's name will be extended with .VaultHelper to enable per connector logging
     */
    public VaultHelper(VaultHelperConfig configProviderConfig, Logger logger) {
        this.configProviderConfig = configProviderConfig;
        this.log = LoggerFactory.getLogger(logger.getName() + ".VaultHelper");
    }

    /**
     * Runs a test getData command to retrieve the data vault if a test path is provided in the config
     */
    public void testConnection() {
        configProviderConfig.getTestPath().ifPresent(path -> {
            LogicalResponse testReponse = this.getData(path);
            log.info("Retrieved test data from {}. Received : {}", path, testReponse.getDataObject().toString(WriterConfig.PRETTY_PRINT));
        });
    }

    /**
     * Performs the vault call to access the resources, or throws a VaultConfigurationProviderException when a failure occurred
     *
     * @param path The path to the secret
     * @return A Vault LogicalResponse object containing the call result.
     */
    protected LogicalResponse getData(String path) {
        log.debug("Getting config and Vault Config to retrieve data from {}", path);
        VaultConfig vaultConfig = configProviderConfig.getVaultConfig();

        log.trace("Creating vault");
        final Vault vault = createVault(vaultConfig);
        log.trace("Logging into Vault");
        AuthResponse response = checkRestResponse(configProviderConfig.login(vault));
        vaultConfig.token(response.getAuthClientToken());

        try {
            return checkRestResponse(vault.logical().read(path));
        } catch (VaultException e) {
            throw new VaultConfigurationProviderException(e);
        } finally {
            try {
                vault.auth().revokeSelf();
            } catch (VaultException e) {
                log.warn("Could not revoke own authentication", e);
            }
        }
    }

    /**
     * Checks the VaultResponse object rest status code for state 200 or 204. Any other code will result in an exception
     *
     * @param response An implementation of the VaultResponse object
     * @param <T>      A type extending the VaultResponse object
     * @return the response object given in the response parameter
     */

    protected <T extends VaultResponse> T checkRestResponse(T response) {
        if (response == null || response.getRestResponse() == null) {
            throw new VaultConfigurationProviderException("Did not receive a response");
        }
        if (response.getRestResponse().getStatus() != 200 && response.getRestResponse().getStatus() != 204) {
            throw new VaultConfigurationProviderException("The rest response returned a not successful status code " + response.getRestResponse().getStatus());
        }
        return response;
    }

    /**
     * Create a new Vault object
     *
     * @param vaultConfig the configuration for the vault
     * @return A newly constructed vault object
     */
    protected Vault createVault(VaultConfig vaultConfig) {
        return new Vault(vaultConfig);
    }

}
