package io.axual.utilities.config.providers;

/*-
 * ========================LICENSE_START=================================
 * vault-config-provider
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import com.bettercloud.vault.SslConfig;
import com.bettercloud.vault.Vault;
import com.bettercloud.vault.VaultConfig;
import com.bettercloud.vault.VaultException;
import com.bettercloud.vault.response.AuthResponse;

import org.apache.kafka.common.config.AbstractConfig;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.types.Password;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import io.axual.utilities.config.providers.exceptions.VaultConfigurationException;
import io.axual.utilities.config.providers.exceptions.VaultConfigurationProviderException;

/**
 * This class extends the AbstractConfig and contains the configuraton definitions required to instantiate a VaultHelper.
 */
public class VaultHelperConfig extends AbstractConfig {
    public static final Logger LOG = LoggerFactory.getLogger(VaultHelperConfig.class);

    /**
     * The URL where HashiCorp Vault can be reached
     */
    public static final String VAULT_ADDRESS_CONFIG = "address";
    private static final String VAULT_ADDRESS_DOC = "The URL where HashiCorp Vault can be reached";

    /**
     * The number of seconds to wait before giving up on establishing an HTTP(S) connection to the Vault server.
     */
    public static final String VAULT_TIMEOUT_OPEN_CONFIG = "timeout.open";
    static final Integer VAULT_TIMEOUT_OPEN_DEFAULT = 30;
    private static final String VAULT_TIMEOUT_OPEN_DOC = "The number of seconds to wait before giving up on establishing an HTTP(S) connection to the Vault server.";

    /**
     * After an HTTP(S) connection has already been established, this is the number of seconds to wait for all data to finish downloading.
     */
    public static final String VAULT_TIMEOUT_READ_CONFIG = "timeout.read";
    static final Integer VAULT_TIMEOUT_READ_DEFAULT = 30;
    private static final String VAULT_TIMEOUT_READ_DOC = "After an HTTP(S) connection has already been established, this is the number of seconds to wait for all data to finish downloading.";

    /**
     * Sets a global namespace to the Vault server instance.
     */
    public static final String VAULT_NAMESPACE_CONFIG = "namespace";
    static final String VAULT_NAMESPACE_DEFAULT = null;
    private static final String VAULT_NAMESPACE_DOC = "Sets a global namespace to the Vault server instance.";

    /**
     * Sets the KV Secrets Engine version of the Vault server instance.
     */
    public static final String VAULT_GLOBAL_ENGINE_VERSION_CONFIG = "global.engine.version";
    static final Integer VAULT_GLOBAL_ENGINE_VERSION_DEFAULT = 2;
    private static final String VAULT_GLOBAL_ENGINE_VERSION_DOC = "Sets the KV Secrets Engine version of the Vault server instance.";

    /**
     * Set the "path depth" of the prefix path.
     *
     * @see #VAULT_PREFIX_PATH_CONFIG
     */
    public static final String VAULT_PREFIX_PATH_DEPTH_CONFIG = "prefix.path.depth";
    static final Integer VAULT_PREFIX_PATH_DEPTH_DEFAULT = 1;
    private static final String VAULT_PREFIX_PATH_DEPTH_DOC = "Set the \"path depth\" of the prefix path.";

    /**
     * Set the "path depth" of the prefix path by specifying the path. <br>
     * example: /a/b/c would result in a prefix path depth of 3
     *
     * @see #VAULT_PREFIX_PATH_DEPTH_CONFIG
     */
    public static final String VAULT_PREFIX_PATH_CONFIG = "prefix.path";
    static final String VAULT_PREFIX_PATH_DEFAULT = null;
    private static final String VAULT_PREFIX_PATH_DOC = "Set the \"path depth\" of the prefix path by specifying the path. /a/b/c would result in a prefix path depth of 3.";

    // SSL Configuration
    private static final String VAULT_SSL_PREFIX = "ssl";

    /**
     * Determines whether or not HTTPS connections to the Vault server should verify that a valid SSL certificate is being used.
     */
    public static final String VAULT_SSL_VERIFY_CONFIG = VAULT_SSL_PREFIX + ".verify";
    static final boolean VAULT_SSL_VERIFY_DEFAULT = true;
    private static final String VAULT_SSL_VERIFY_DOC = "Determines whether or not HTTPS connections to the Vault server should verify that a valid SSL certificate is being used.";

    /**
     * The path to a JKS keystore file, containing the Vault TLS Certificate or Certificate Authorities.
     */
    public static final String VAULT_SSL_TRUSTSTORE_LOCATION_CONFIG = VAULT_SSL_PREFIX + ".truststore.location";
    static final String VAULT_SSL_TRUSTSTORE_LOCATION_DEFAULT = null;
    private static final String VAULT_SSL_TRUSTSTORE_LOCATION_DOC = "The path to a JKS keystore file, containing the Vault TLS Certificate or Certificate Authorities.";

    /**
     * he password to access the JKS keystore file containing the Vault TLS Certificate or Certificate Authorities.
     */
    public static final String VAULT_SSL_TRUSTSTORE_PASSWORD_CONFIG = VAULT_SSL_PREFIX + ".truststore.password";
    static final String VAULT_SSL_TRUSTSTORE_PASSWORD_DEFAULT = null;
    private static final String VAULT_SSL_TRUSTSTORE_PASSWORD_DOC = "The password to access the JKS keystore file containing the Vault TLS Certificate or Certificate Authorities.";

    /**
     * The value to use for enabling AppRole authentication.
     *
     * @see #VAULT_AUTH_METHOD_CONFIG
     */
    public static final String VAULT_AUTH_METHOD_APPROLE = "APPROLE";

    /**
     * Determines the authentication method to use for communicating with Vault.<br>
     * Valid values is APPROLE<br>
     * Default value is APPROLE
     *
     * @see #VAULT_AUTH_METHOD_APPROLE
     */
    public static final String VAULT_AUTH_METHOD_CONFIG = "auth.method";
    static final String VAULT_AUTH_METHOD_DEFAULT = VAULT_AUTH_METHOD_APPROLE;
    private static final String VAULT_AUTH_METHOD_DOC = "The authentication method to use. Valid values is " + VAULT_AUTH_METHOD_APPROLE;

    /**
     * The path on which the authentication is performed when using AppRole authentication, following the "/v1/auth/" prefix (e.g. "approle").
     */
    public static final String VAULT_CREDENTIAL_APPROLE_PATH_CONFIG = "approle.path";
    static final String VAULT_CREDENTIAL_APPROLE_PATH_DEFAULT = "approle";
    private static final String VAULT_CREDENTIAL_APPROLE_PATH_DOC = "The path on which the authentication is performed when using AppRole authentication, following the \"/v1/auth/\" prefix (e.g. \"approle\").";

    /**
     * The Vault role id to use for communicating with Vault when using AppRole authentication.
     */
    public static final String VAULT_CREDENTIAL_APPROLE_ROLE_ID_CONFIG = "approle.role.id";
    static final String VAULT_CREDENTIAL_APPROLE_ID_DEFAULT = null;
    private static final String VAULT_CREDENTIAL_APPROLE_ID_DOC = "The Vault role id to use for communicating with Vault when using AppRole authentication.";

    /**
     * The Vault secret id to use for communicating with Vault when using AppRole authentication.
     */
    public static final String VAULT_CREDENTIAL_APPROLE_SECRET_ID_CONFIG = "approle.secret.id";
    static final String VAULT_CREDENTIAL_APPROLE_SECRET_ID_DEFAULT = null;
    private static final String VAULT_CREDENTIAL_APPROLE_SECRET_ID_DOC = "The Vault secret id to use for communicating with Vault when using AppRole authentication.";

    /**
     * The path of the data to retrieve from Vault during configuration.
     */
    public static final String VAULT_TEST_PATH_CONFIG = "test.path";
    static final String VAULT_TEST_PATH_DEFAULT = null;
    private static final String VAULT_TEST_PATH_DOC = "The path of the data to retrieve from Vault during configuration.";

    private static final ConfigDef CONFIG_DEF = addVaultHelperConfigDefinitions(new ConfigDef());

    /**
     * Adds the Configuration Definitions required by the VaultHelperConfig to an existing Configuration Definition.
     *
     * @param configDef The Configuration Definition to extend.
     * @return The enriched Configuration Definition.
     */
    public static ConfigDef addVaultHelperConfigDefinitions(ConfigDef configDef) {

        // Configuration Provider Options
        configDef
                .define(VAULT_TEST_PATH_CONFIG, ConfigDef.Type.STRING, VAULT_TEST_PATH_DEFAULT, ConfigDef.Importance.LOW, VAULT_TEST_PATH_DOC)
        ;

        // Configure the vault connection
        configDef
                .define(VAULT_ADDRESS_CONFIG, ConfigDef.Type.STRING, ConfigDef.Importance.HIGH, VAULT_ADDRESS_DOC)
                .define(VAULT_TIMEOUT_OPEN_CONFIG, ConfigDef.Type.INT, VAULT_TIMEOUT_OPEN_DEFAULT, ConfigDef.Importance.MEDIUM, VAULT_TIMEOUT_OPEN_DOC)
                .define(VAULT_TIMEOUT_READ_CONFIG, ConfigDef.Type.INT, VAULT_TIMEOUT_READ_DEFAULT, ConfigDef.Importance.MEDIUM, VAULT_TIMEOUT_READ_DOC)
                .define(VAULT_NAMESPACE_CONFIG, ConfigDef.Type.STRING, VAULT_NAMESPACE_DEFAULT, ConfigDef.Importance.LOW, VAULT_NAMESPACE_DOC)
                .define(VAULT_GLOBAL_ENGINE_VERSION_CONFIG, ConfigDef.Type.INT, VAULT_GLOBAL_ENGINE_VERSION_DEFAULT, ConfigDef.Importance.LOW, VAULT_GLOBAL_ENGINE_VERSION_DOC)
                .define(VAULT_PREFIX_PATH_DEPTH_CONFIG, ConfigDef.Type.INT, VAULT_PREFIX_PATH_DEPTH_DEFAULT, ConfigDef.Importance.LOW, VAULT_PREFIX_PATH_DEPTH_DOC)
                .define(VAULT_PREFIX_PATH_CONFIG, ConfigDef.Type.STRING, VAULT_PREFIX_PATH_DEFAULT, ConfigDef.Importance.LOW, VAULT_PREFIX_PATH_DOC);

        // Configure the SSL connection for Vault
        configDef
                .define(VAULT_SSL_VERIFY_CONFIG, ConfigDef.Type.BOOLEAN, VAULT_SSL_VERIFY_DEFAULT, ConfigDef.Importance.MEDIUM, VAULT_SSL_VERIFY_DOC)
                .define(VAULT_SSL_TRUSTSTORE_LOCATION_CONFIG, ConfigDef.Type.STRING, VAULT_SSL_TRUSTSTORE_LOCATION_DEFAULT, ConfigDef.Importance.MEDIUM, VAULT_SSL_TRUSTSTORE_LOCATION_DOC)
                .define(VAULT_SSL_TRUSTSTORE_PASSWORD_CONFIG, ConfigDef.Type.PASSWORD, VAULT_SSL_TRUSTSTORE_PASSWORD_DEFAULT, ConfigDef.Importance.MEDIUM, VAULT_SSL_TRUSTSTORE_PASSWORD_DOC)
        ;

        // Authentication methods
        configDef
                .define(VAULT_AUTH_METHOD_CONFIG, ConfigDef.Type.STRING, VAULT_AUTH_METHOD_DEFAULT, ConfigDef.Importance.HIGH, VAULT_AUTH_METHOD_DOC)
                .define(VAULT_CREDENTIAL_APPROLE_PATH_CONFIG, ConfigDef.Type.STRING, VAULT_CREDENTIAL_APPROLE_PATH_DEFAULT, ConfigDef.Importance.LOW, VAULT_CREDENTIAL_APPROLE_PATH_DOC)
                .define(VAULT_CREDENTIAL_APPROLE_ROLE_ID_CONFIG, ConfigDef.Type.STRING, VAULT_CREDENTIAL_APPROLE_ID_DEFAULT, ConfigDef.Importance.LOW, VAULT_CREDENTIAL_APPROLE_ID_DOC)
                .define(VAULT_CREDENTIAL_APPROLE_SECRET_ID_CONFIG, ConfigDef.Type.PASSWORD, VAULT_CREDENTIAL_APPROLE_SECRET_ID_DEFAULT, ConfigDef.Importance.LOW, VAULT_CREDENTIAL_APPROLE_SECRET_ID_DOC)
        ;
        return configDef;
    }

    /**
     * Verifies that the provided ConfigDef instance contains the definition names required for the VaultHelperConfig.<br>
     * If the verification fails an IllegalArgumentException is thrown.
     *
     * @param definition The definition to verify
     * @return the verified definition
     */
    protected static ConfigDef verifyVaultHelperConfigDefinition(ConfigDef definition) {
        Set<String> configKeys = definition.names();
        Set<String> missing = CONFIG_DEF.names().stream()
                .filter(name -> !configKeys.contains(name))
                .collect(Collectors.toSet());

        if (!missing.isEmpty()) {
            throw new IllegalArgumentException("ConfigDef is missing configurations : " + String.join(",", missing));
        }

        return definition;
    }

    /**
     * Construct a VaultHelperConfig with the default Configuration Definition.
     *
     * @param originals The properties used to construct the VaultHelperConfig
     */
    public VaultHelperConfig(Map<?, ?> originals) {
        this(CONFIG_DEF, originals, true);
    }

    /**
     * Construct a VaultHelperConfig with the default Configuration Definition.
     *
     * @param originals The properties used to construct the VaultHelperConfig
     * @param doLog     Log the configuration properties
     */
    public VaultHelperConfig(Map<?, ?> originals, boolean doLog) {
        this(CONFIG_DEF, originals, doLog);
    }

    /**
     * Construct a VaultHelperConfig with a provided Configuration Definition.<br>
     * The provided definition will be verified is it contains the proper fields.<br>
     *
     * This constructor can be used for extending the VaultHelperConfig
     *
     * @param definition A provided custom Configuration Definition
     * @param originals  The properties used to construct the VaultHelperConfig
     */
    public VaultHelperConfig(ConfigDef definition, Map<?, ?> originals) {
        this(definition, originals, true);
    }

    private final Optional<String> testPath;
    private final Optional<String> address;
    private final Optional<Integer> openTimeout;
    private final Optional<Integer> readTimeout;
    private final Optional<String> namespace;
    private final Optional<Integer> globalEngineVersion;
    private final Optional<Integer> prefixPathDepth;
    private final Optional<String> prefixPath;
    private final Optional<Boolean> sslVerify;
    private final Optional<String> truststoreLocation;
    private final Optional<Password> truststorePassword;
    private final Optional<String> authMethod;
    private final Optional<String> appRolePath;
    private final Optional<String> appRoleId;
    private final Optional<Password> appRoleSecretId;

    /**
     * Construct a VaultHelperConfig with a provided Configuration Definition.<br>
     * The provided definition will be verified is it contains the proper fields.<br>
     *
     * This constructor can be used for extending the VaultHelperConfig
     *
     * @param definition A provided custom Configuration Definition
     * @param originals  The properties used to construct the VaultHelperConfig
     * @param doLog      Log the configuration properties
     */
    public VaultHelperConfig(ConfigDef definition, Map<?, ?> originals, boolean doLog) {
        super(verifyVaultHelperConfigDefinition(definition), originals, doLog);

        testPath = getOptionalString(VAULT_TEST_PATH_CONFIG);
        address = getOptionalString(VAULT_ADDRESS_CONFIG);
        openTimeout = getOptionalInt(VAULT_TIMEOUT_OPEN_CONFIG);
        readTimeout = getOptionalInt(VAULT_TIMEOUT_READ_CONFIG);
        namespace = getOptionalString(VAULT_NAMESPACE_CONFIG);
        globalEngineVersion = getOptionalInt(VAULT_GLOBAL_ENGINE_VERSION_CONFIG);
        prefixPathDepth = getOptionalInt(VAULT_PREFIX_PATH_DEPTH_CONFIG);
        prefixPath = getOptionalString(VAULT_PREFIX_PATH_CONFIG);
        sslVerify = getOptionalBoolean(VAULT_SSL_VERIFY_CONFIG);
        truststoreLocation = getOptionalString(VAULT_SSL_TRUSTSTORE_LOCATION_CONFIG);
        truststorePassword = getOptionalPassword(VAULT_SSL_TRUSTSTORE_PASSWORD_CONFIG);
        authMethod = getOptionalString(VAULT_AUTH_METHOD_CONFIG);
        appRolePath = getOptionalString(VAULT_CREDENTIAL_APPROLE_PATH_CONFIG);
        appRoleId = getOptionalString(VAULT_CREDENTIAL_APPROLE_ROLE_ID_CONFIG);
        appRoleSecretId = getOptionalPassword(VAULT_CREDENTIAL_APPROLE_SECRET_ID_CONFIG);
    }

    private VaultConfig vaultConfig = null;

    /**
     * Create and return a VaultConfig object using the provided configuration.<br>
     *
     * The method will always return the same object.
     *
     * @return A VaultConfig object ready to use for Vault operations
     */
    public synchronized VaultConfig getVaultConfig() {
        if (this.vaultConfig == null) {
            VaultConfig newVaultConfig = new VaultConfig();

            createSslConfig().ifPresent(newVaultConfig::sslConfig);
            address.ifPresent(newVaultConfig::address);
            globalEngineVersion.ifPresent(newVaultConfig::engineVersion);
            openTimeout.ifPresent(newVaultConfig::openTimeout);
            readTimeout.ifPresent(newVaultConfig::readTimeout);
            prefixPathDepth.ifPresent(newVaultConfig::prefixPathDepth);
            prefixPath.ifPresent(newVaultConfig::prefixPath);

            try {
                if (namespace.isPresent()) {
                    newVaultConfig.nameSpace(namespace.get());
                }

                newVaultConfig.build();
            } catch (VaultException ve) {
                throw new VaultConfigurationException(ve);
            }
            this.vaultConfig = newVaultConfig;
        }
        return this.vaultConfig;
    }

    /**
     * Creates the SSL Configuration required for the VaultConfig object
     *
     * @return An optional SslConfig object to be used by VaultConfig
     */
    private Optional<SslConfig> createSslConfig() {
        Map<String, Object> configs = this.originalsWithPrefix(VAULT_SSL_PREFIX, false);
        if (configs.isEmpty()) {
            return Optional.empty();
        } else {
            SslConfig sslConfig = new SslConfig();
            sslVerify.ifPresent(sslConfig::verify);
            handleKeystore(this::getSslTruststoreLocation, this::getSslTruststorePassword, (ks, pass) -> sslConfig.trustStore(ks));
            return Optional.of(sslConfig);
        }
    }

    /**
     * Helper method to extract and load KeyStore data and supply it to another component
     *
     * @param locationSupplier A method to supply the location of the keystore file.
     * @param passwordSupplier A method to supply the required password for the keystore file.
     * @param consumer         The operation to load the KeyStore and the password.
     */
    private void handleKeystore(Supplier<Optional<String>> locationSupplier, Supplier<Optional<Password>> passwordSupplier, BiConsumer<KeyStore, String> consumer) {
        Optional<String> optionalLocation = locationSupplier.get();
        Optional<Password> optionalPassword = passwordSupplier.get();

        optionalLocation.ifPresent(location -> {
                    final String password = optionalPassword.map(Password::value).orElse(null);

                    try (FileInputStream inputStream = new FileInputStream(location)) {
                        KeyStore keyStore = KeyStore.getInstance("JKS");
                        keyStore.load(inputStream, password == null ? null : password.toCharArray());

                        consumer.accept(keyStore, password);
                    } catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException e) {
                        throw new VaultConfigurationProviderException("Could not create keystore object", e);
                    }
                }
        );
    }

    /**
     * Login to the Vault server with the provided authentication method and credentials.
     *
     * @param vault The Vault object constructed from the VaultConfig
     * @return The Vault Authentication Response
     */
    public synchronized AuthResponse login(Vault vault) {
        if (this.authMethod.isPresent()) {
            final String authenticationMethod = this.authMethod.get();
            AuthResponse authResponse;
            if (VAULT_AUTH_METHOD_APPROLE.equals(authenticationMethod)) {
                LOG.debug("Using APPROLE authentication");
                if (appRoleId.isPresent() && appRoleSecretId.isPresent() && appRolePath.isPresent()) {
                    try {
                        authResponse = vault.auth().loginByAppRole(appRolePath.get(), appRoleId.get(), appRoleSecretId.get().value());
                    } catch (VaultException ve) {
                        throw new VaultConfigurationException("Could not log in with AppRole Authentication", ve);
                    }
                } else {
                    throw new VaultConfigurationException("Path, role id and secret id are required for AppRole authentication");
                }

            } else {
                throw new VaultConfigurationException("Unknown Authentication Method set " + authenticationMethod);
            }

            return authResponse;

        } else {
            throw new VaultConfigurationException("No Authentication Method set");
        }
    }

    /**
     * Helper method to retrieve a String value which might not be set in the configuration
     *
     * @param key The configuration key for the requested value
     * @return The Optional object packaging the requested configuration
     */
    protected Optional<String> getOptionalString(final String key) {
        if (this.values().containsKey(key)) {
            return Optional.ofNullable(getString(key));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Helper method to retrieve an Integer value which might not be set in the configuration
     *
     * @param key The configuration key for the requested value
     * @return The Optional object packaging the requested configuration
     */
    protected Optional<Integer> getOptionalInt(final String key) {
        if (this.values().containsKey(key)) {
            return Optional.ofNullable(getInt(key));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Helper method to retrieve a Boolean value which might not be set in the configuration
     *
     * @param key The configuration key for the requested value
     * @return The Optional object packaging the requested configuration
     */
    protected Optional<Boolean> getOptionalBoolean(final String key) {
        if (this.values().containsKey(key)) {
            return Optional.ofNullable(getBoolean(key));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Helper method to retrieve a Password value which might not be set in the configuration
     *
     * @param key The configuration key for the requested value
     * @return The Optional object packaging the requested configuration
     */
    public Optional<Password> getOptionalPassword(final String key) {
        if (this.values().containsKey(key)) {
            return Optional.ofNullable(getPassword(key));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Returns the test path packaged in an Optional object.
     *
     * @return The optional test path
     * @see #VAULT_TEST_PATH_CONFIG
     */
    public Optional<String> getTestPath() {
        return testPath;
    }

    /**
     * Returns the Vault server address packaged in an Optional object.
     *
     * @return The Vault address
     * @see #VAULT_ADDRESS_CONFIG
     */
    public Optional<String> getAddress() {
        return address;
    }

    /**
     * Returns the timeout for open operations packaged in an Optional object.
     *
     * @return The optional open timeout
     * @see #VAULT_TIMEOUT_OPEN_CONFIG
     */
    public Optional<Integer> getOpenTimeout() {
        return openTimeout;
    }

    /**
     * Returns the timeout for read operations packaged in an Optional object.
     *
     * @return The optional read timeout
     * @see #VAULT_TIMEOUT_READ_CONFIG
     */
    public Optional<Integer> getReadTimeout() {
        return readTimeout;
    }

    /**
     * Returns the Vault namespace packaged in an Optional object.
     *
     * @return The optional namespace
     * @see #VAULT_NAMESPACE_CONFIG
     */
    public Optional<String> getNameSpace() {
        return namespace;
    }

    /**
     * Returns the Vault Global Engine version packaged in an Optional object.
     *
     * @return The optional value for the Global Engine version
     * @see #VAULT_GLOBAL_ENGINE_VERSION_CONFIG
     */
    public Optional<Integer> getGlobalEngineVersion() {
        return globalEngineVersion;
    }

    /**
     * Returns the Vault prefix path depth in an Optional object.
     *
     * @return The optional prefix path depth
     * @see #VAULT_PREFIX_PATH_DEPTH_CONFIG
     */
    public Optional<Integer> getPrefixPathDepth() {
        return prefixPathDepth;
    }

    /**
     * Returns the Vault prefix path in an Optional object.
     *
     * @return The optional prefix path
     * @see #VAULT_PREFIX_PATH_CONFIG
     */
    public Optional<String> getPrefixPath() {
        return prefixPath;
    }

    /**
     * Returns the boolean to control SSL verification in an Optional object.
     *
     * @return The optional SSL Verification boolean
     * @see #VAULT_SSL_VERIFY_CONFIG
     */
    public Optional<Boolean> getSslVerify() {
        return sslVerify;
    }

    /**
     * Returns the location of the truststore in an Optional object.
     *
     * @return The optional truststore location
     * @see #VAULT_SSL_TRUSTSTORE_LOCATION_CONFIG
     */
    public Optional<String> getSslTruststoreLocation() {
        return truststoreLocation;
    }

    /**
     * Returns the password of the truststore in an Optional object.
     *
     * @return The optional truststore password
     * @see #VAULT_SSL_TRUSTSTORE_PASSWORD_CONFIG
     */
    public Optional<Password> getSslTruststorePassword() {
        return truststorePassword;
    }

    /**
     * Returns the authentication method in an Optional object.
     *
     * @return The optional authentication method
     * @see #VAULT_AUTH_METHOD_CONFIG
     */
    public Optional<String> getAuthMethod() {
        return authMethod;
    }

    /**
     * Returns the path to use for AppRole login in an Optional object.
     *
     * @return The optional AppRole authentication path
     * @see #VAULT_CREDENTIAL_APPROLE_PATH_CONFIG
     */
    public Optional<String> getAppRolePath() {
        return appRolePath;
    }

    /**
     * Returns the Role ID to use for AppRole login in an Optional object.
     *
     * @return The optional AppRole Role ID
     * @see #VAULT_CREDENTIAL_APPROLE_ROLE_ID_CONFIG
     */
    public Optional<String> getAppRoleId() {
        return appRoleId;
    }

    /**
     * Returns the Secret ID to use for AppRole login in an Optional object.
     *
     * @return The optional AppRole Secret ID
     * @see #VAULT_CREDENTIAL_APPROLE_SECRET_ID_CONFIG
     */
    public Optional<Password> getAppRoleSecretId() {
        return appRoleSecretId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VaultHelperConfig)) return false;
        if (!super.equals(o)) return false;
        VaultHelperConfig config = (VaultHelperConfig) o;
        return testPath.equals(config.testPath) &&
                address.equals(config.address) &&
                openTimeout.equals(config.openTimeout) &&
                readTimeout.equals(config.readTimeout) &&
                namespace.equals(config.namespace) &&
                globalEngineVersion.equals(config.globalEngineVersion) &&
                prefixPathDepth.equals(config.prefixPathDepth) &&
                prefixPath.equals(config.prefixPath) &&
                sslVerify.equals(config.sslVerify) &&
                truststoreLocation.equals(config.truststoreLocation) &&
                truststorePassword.equals(config.truststorePassword) &&
                authMethod.equals(config.authMethod) &&
                appRolePath.equals(config.appRolePath) &&
                appRoleId.equals(config.appRoleId) &&
                appRoleSecretId.equals(config.appRoleSecretId) &&
                Objects.equals(vaultConfig, config.vaultConfig);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), testPath, address, openTimeout, readTimeout, namespace, globalEngineVersion, prefixPathDepth, prefixPath, sslVerify, truststoreLocation, truststorePassword, authMethod, appRolePath, appRoleId, appRoleSecretId, vaultConfig);
    }
}
