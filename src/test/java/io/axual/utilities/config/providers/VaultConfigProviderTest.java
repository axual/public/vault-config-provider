package io.axual.utilities.config.providers;

/*-
 * ========================LICENSE_START=================================
 * vault-config-provider
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import com.bettercloud.vault.response.LogicalResponse;

import org.apache.commons.compress.utils.Sets;
import org.apache.kafka.common.config.ConfigData;
import org.apache.kafka.common.config.ConfigException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import io.axual.utilities.config.providers.exceptions.VaultConfigurationRetrieveException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class VaultConfigProviderTest {
    @Captor
    ArgumentCaptor<String> pathCaptor;

    @Mock
    LogicalResponse mockResponse;

    @Mock
    VaultHelper mockVaultHelper;

    @Test
    void reconfigure() {
        VaultConfigProvider toTest = spy(new VaultConfigProvider(mockVaultHelper));
        Map<String, Object> props = new HashMap<>();
        props.put(VaultHelperConfig.VAULT_ADDRESS_CONFIG, "http://SomeAddress");
        props.put(VaultHelperConfig.VAULT_CREDENTIAL_APPROLE_ROLE_ID_CONFIG, "appID");
        props.put(VaultHelperConfig.VAULT_CREDENTIAL_APPROLE_SECRET_ID_CONFIG, "secretId");
        toTest.configure(props);

        verify(toTest, never()).createVaultHelper(any());
        verify(toTest, never()).get(anyString());
        verify(toTest, never()).get(anyString(), anySet());
    }

    @Test
    void configure() {
        final String EXPECTED_PATH = "/over/there";

        VaultConfigProvider toTest = spy(new VaultConfigProvider());

        Map<String, Object> props = new HashMap<>();
        props.put(VaultHelperConfig.VAULT_ADDRESS_CONFIG, "http://SomeAddress");
        props.put(VaultHelperConfig.VAULT_CREDENTIAL_APPROLE_ROLE_ID_CONFIG, "appID");
        props.put(VaultHelperConfig.VAULT_CREDENTIAL_APPROLE_SECRET_ID_CONFIG, "secretId");
        props.put(VaultHelperConfig.VAULT_TEST_PATH_CONFIG, EXPECTED_PATH);

        doReturn(mockVaultHelper).when(toTest).createVaultHelper(any());

        toTest.configure(props);
        props.remove(VaultHelperConfig.VAULT_TEST_PATH_CONFIG);
        toTest.configure(props);

        verify(toTest, times(1)).createVaultHelper(any());
        verify(toTest, never()).get(anyString());
        verify(toTest, never()).get(anyString(), anySet());
    }


    @Test
    void configureIncorrect() {
        VaultConfigProvider toTest = new VaultConfigProvider();
        Map<String, Object> props = new HashMap<>();
        assertThrows(ConfigException.class, () -> toTest.configure(props));
    }


    @Test
    void getAllKeys() {
        String path = "/over/there";
        Map<String, String> toReturn = new HashMap<>();
        toReturn.put("key1", "value1");
        toReturn.put("key2", "value2");
        toReturn.put("key3", "value3");

        runGet(toReturn, toReturn.keySet(), path, provider -> provider.get(path));
    }

    @Test
    void getSelectKeys() {
        String path = "/over/there";
        HashSet<String> keySet = Sets.newHashSet("getKey1", "getKey2", "getKey3");
        Map<String, String> toReturn = new HashMap<>();
        toReturn.put("key1", "value1");
        toReturn.put("key2", "value2");
        toReturn.put("key3", "value3");

        for (String newKey : keySet) {
            toReturn.put(newKey, "value of " + newKey);
        }

        runGet(toReturn, keySet, path, provider -> provider.get(path, keySet));
    }

    @Test
    void getAllKeysWithEmptySelectSet() {
        String path = "/over/there";
        HashSet<String> keySet = Sets.newHashSet("", null, " ");
        Map<String, String> toReturn = new HashMap<>();
        toReturn.put("key1", "value1");
        toReturn.put("key2", "value2");
        toReturn.put("key3", "value3");

        runGet(toReturn, toReturn.keySet(), path, provider -> provider.get(path, keySet));
    }

    @Test
    void getSelectKeysMissing() {
        String path = "/over/there";
        HashSet<String> keySet = Sets.newHashSet("getKey1", "getKey2", "getKey3");
        Map<String, String> toReturn = new HashMap<>();
        toReturn.put("key1", "value1");
        toReturn.put("key2", "value2");
        toReturn.put("key3", "value3");

        for (String newKey : keySet) {
            toReturn.put(newKey, "value of " + newKey);
        }

        keySet.add("missingKey");
        assertThrows(VaultConfigurationRetrieveException.class, () -> runGet(toReturn, keySet, path, provider -> provider.get(path, keySet)));
    }

    void runGet(Map<String, String> toReturn, Set<String> expected, String path, Function<VaultConfigProvider, ConfigData> function) {
        doReturn(toReturn).when(mockResponse).getData();

        VaultConfigProvider toTest = spy(new VaultConfigProvider(mockVaultHelper));
        doReturn(mockResponse).when(mockVaultHelper).getData(anyString());

        ConfigData data = function.apply(toTest);

        verify(mockVaultHelper, times(1)).getData(pathCaptor.capture());
        assertEquals(path, pathCaptor.getValue());

        assertNotNull(data);
        Map<String, String> received = data.data();
        assertNotNull(received);
        for (String key : expected) {
            assertTrue(received.containsKey(key), "Expecting key " + key);
            assertEquals(toReturn.get(key), received.get(key), "Expecting the same value for key " + key);
        }
    }

    @Test
    void closeProvider() {
        VaultConfigProvider toTest = new VaultConfigProvider(mockVaultHelper);
        assertDoesNotThrow(() -> toTest.close());
    }
}
