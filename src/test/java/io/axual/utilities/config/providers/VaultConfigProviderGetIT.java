package io.axual.utilities.config.providers;

/*-
 * ========================LICENSE_START=================================
 * vault-config-provider
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.config.ConfigData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.CollectionUtils;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import io.axual.utilities.config.providers.util.VaultTestContainer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Testcontainers
class VaultConfigProviderGetIT {

    @Container
    VaultTestContainer vaultContainer = VaultTestContainer.getInstance();

    @BeforeEach
    void prepare() {
        vaultContainer.start();
    }

    @Test
    void getAll() {
        final String address = vaultContainer.getAddress();
        final String secretId = vaultContainer.getSecretId();
        final String roleId = vaultContainer.getRoleId();

        Map<String, Object> props = new HashMap<>();
        props.put(VaultHelperConfig.VAULT_ADDRESS_CONFIG, address);
        props.put(VaultHelperConfig.VAULT_AUTH_METHOD_CONFIG, VaultHelperConfig.VAULT_AUTH_METHOD_APPROLE);
        props.put(VaultHelperConfig.VAULT_CREDENTIAL_APPROLE_ROLE_ID_CONFIG, roleId);
        props.put(VaultHelperConfig.VAULT_CREDENTIAL_APPROLE_SECRET_ID_CONFIG, secretId);

        VaultConfigProvider provider = new VaultConfigProvider();
        provider.configure(props);

        ConfigData data = provider.get(VaultTestContainer.RESOURCE_DATA_PATH);
        assertNotNull(data);
        Map<String, String> configData = data.data();
        assertNotNull(configData);

        assertTrue(configData.containsKey(VaultTestContainer.RESOURCE_DATA_KEY_ONE));
        assertEquals(VaultTestContainer.RESOURCE_DATA_VALUE_ONE, configData.get(VaultTestContainer.RESOURCE_DATA_KEY_ONE));
        assertTrue(configData.containsKey(VaultTestContainer.RESOURCE_DATA_KEY_TWO));
        assertEquals(VaultTestContainer.RESOURCE_DATA_VALUE_TWO, configData.get(VaultTestContainer.RESOURCE_DATA_KEY_TWO));
        assertTrue(configData.containsKey(VaultTestContainer.RESOURCE_DATA_KEY_THREE));
        assertEquals(VaultTestContainer.RESOURCE_DATA_VALUE_THREE, configData.get(VaultTestContainer.RESOURCE_DATA_KEY_THREE));
    }

    @Test
    void getSome() {
        final String address = vaultContainer.getAddress();
        final String secretId = vaultContainer.getSecretId();
        final String roleId = vaultContainer.getRoleId();

        Map<String, Object> props = new HashMap<>();
        props.put(VaultHelperConfig.VAULT_ADDRESS_CONFIG, address);
        props.put(VaultHelperConfig.VAULT_AUTH_METHOD_CONFIG, VaultHelperConfig.VAULT_AUTH_METHOD_APPROLE);
        props.put(VaultHelperConfig.VAULT_CREDENTIAL_APPROLE_ROLE_ID_CONFIG, roleId);
        props.put(VaultHelperConfig.VAULT_CREDENTIAL_APPROLE_SECRET_ID_CONFIG, secretId);

        VaultConfigProvider provider = new VaultConfigProvider();
        provider.configure(props);

        Set<String> keys = CollectionUtils.toSet(new String[]{VaultTestContainer.RESOURCE_DATA_KEY_ONE, VaultTestContainer.RESOURCE_DATA_KEY_THREE});
        ConfigData data = provider.get(VaultTestContainer.RESOURCE_DATA_PATH, keys);
        assertNotNull(data);
        Map<String, String> configData = data.data();
        assertNotNull(configData);

        assertEquals(keys.size(), configData.size());
        assertTrue(configData.containsKey(VaultTestContainer.RESOURCE_DATA_KEY_ONE));
        assertEquals(VaultTestContainer.RESOURCE_DATA_VALUE_ONE, configData.get(VaultTestContainer.RESOURCE_DATA_KEY_ONE));
        assertTrue(configData.containsKey(VaultTestContainer.RESOURCE_DATA_KEY_THREE));
        assertEquals(VaultTestContainer.RESOURCE_DATA_VALUE_THREE, configData.get(VaultTestContainer.RESOURCE_DATA_KEY_THREE));
    }
}
