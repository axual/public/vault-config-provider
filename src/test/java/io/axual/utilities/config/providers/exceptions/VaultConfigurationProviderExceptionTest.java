package io.axual.utilities.config.providers.exceptions;

/*-
 * ========================LICENSE_START=================================
 * Configuration Provider for HashiCorp Vault
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class VaultConfigurationProviderExceptionTest {

    public static final String EXPECTED_MESSAGE = "message";
    public static final Throwable EXPECTED_CAUSE = new RuntimeException(EXPECTED_MESSAGE);


    @Test
    void testConstructorCause() {
        VaultConfigurationProviderException exception = new VaultConfigurationProviderException(EXPECTED_CAUSE);
        assertEquals(EXPECTED_CAUSE, exception.getCause());
    }

    @Test
    void testConstructorMessage() {
        VaultConfigurationProviderException exception = new VaultConfigurationProviderException(EXPECTED_MESSAGE);
        assertEquals(EXPECTED_MESSAGE, exception.getMessage());
    }

    @Test
    void testConstructorCauseMessage() {
        VaultConfigurationProviderException exception = new VaultConfigurationProviderException(EXPECTED_MESSAGE, EXPECTED_CAUSE);
        assertEquals(EXPECTED_CAUSE, exception.getCause());
        assertEquals(EXPECTED_MESSAGE, exception.getMessage());
    }
}
