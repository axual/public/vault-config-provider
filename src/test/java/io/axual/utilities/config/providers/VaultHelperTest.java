package io.axual.utilities.config.providers;

/*-
 * ========================LICENSE_START=================================
 * Configuration Provider for HashiCorp Vault
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import com.bettercloud.vault.Vault;
import com.bettercloud.vault.VaultConfig;
import com.bettercloud.vault.VaultException;
import com.bettercloud.vault.api.Auth;
import com.bettercloud.vault.api.Logical;
import com.bettercloud.vault.json.JsonObject;
import com.bettercloud.vault.response.AuthResponse;
import com.bettercloud.vault.response.LogicalResponse;
import com.bettercloud.vault.rest.RestResponse;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

import io.axual.utilities.config.providers.exceptions.VaultConfigurationProviderException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class VaultHelperTest {
    private static final Logger LOG = LoggerFactory.getLogger(VaultHelperTest.class);

    @Mock
    VaultHelperConfig mockProviderConfig;

    @Mock
    Vault mockVault;

    @Mock
    VaultConfig mockVaultConfig;

    @Mock
    AuthResponse mockAuthResponse;

    @Mock
    Logical mockLogical;

    @Mock
    Auth mockAuth;

    @Mock
    LogicalResponse mockLogicalResponse;

    @Test
    void getDataSuccessCode200() throws VaultException {
        VaultHelper helper = spy(new VaultHelper(mockProviderConfig, LOG));
        doReturn(mockVaultConfig).when(mockProviderConfig).getVaultConfig();
        doReturn(mockVault).when(helper).createVault(any());
        doReturn(mockAuthResponse).when(mockProviderConfig).login(mockVault);
        doReturn(mockLogical).when(mockVault).logical();
        doReturn(mockAuth).when(mockVault).auth();
        doNothing().when(mockAuth).revokeSelf();
        doReturn(mockLogicalResponse).when(mockLogical).read(anyString());

        doReturn(mockAuthResponse).when(helper).checkRestResponse(eq(mockAuthResponse));
        doReturn(mockLogicalResponse).when(helper).checkRestResponse(eq(mockLogicalResponse));


        final String path = "/path";
        LogicalResponse response = helper.getData("path");
        assertEquals(mockLogicalResponse, response);
    }

    @Test
    void getDataSuccessRestException() throws VaultException {
        VaultHelper helper = spy(new VaultHelper(mockProviderConfig, LOG));
        doReturn(mockVaultConfig).when(mockProviderConfig).getVaultConfig();
        doReturn(mockVault).when(helper).createVault(any());
        doReturn(mockAuthResponse).when(mockProviderConfig).login(mockVault);
        doReturn(mockLogical).when(mockVault).logical();
        doReturn(mockAuth).when(mockVault).auth();
        doNothing().when(mockAuth).revokeSelf();

        doThrow(new VaultException("")).when(mockLogical).read(anyString());

        doReturn(mockAuthResponse).when(helper).checkRestResponse(eq(mockAuthResponse));


        final String path = "/path";
        assertThrows(VaultConfigurationProviderException.class, () -> helper.getData("path"));
    }

    @Test
    void checkRestResponseOk200() {
        VaultHelper helper = spy(new VaultHelper(mockProviderConfig, LOG));
        final int httpCode = 200;
        final RestResponse restResponse = new RestResponse(httpCode, "", null);
        doReturn(restResponse).when(mockLogicalResponse).getRestResponse();
        assertEquals(mockLogicalResponse, helper.checkRestResponse(mockLogicalResponse));
    }

    @Test
    void checkRestResponseOk204() {
        VaultHelper helper = spy(new VaultHelper(mockProviderConfig, LOG));
        final int httpCode = 204;
        final RestResponse restResponse = new RestResponse(httpCode, "", null);
        doReturn(restResponse).when(mockLogicalResponse).getRestResponse();
        assertEquals(mockLogicalResponse, helper.checkRestResponse(mockLogicalResponse));
    }

    @Test
    void checkRestResponseNotOk404() {
        VaultHelper helper = spy(new VaultHelper(mockProviderConfig, LOG));
        final int httpCode = 404;
        final RestResponse restResponse = new RestResponse(httpCode, "", null);
        doReturn(restResponse).when(mockLogicalResponse).getRestResponse();
        assertThrows(VaultConfigurationProviderException.class, () -> helper.checkRestResponse(mockLogicalResponse));
    }

    @Test
    void checkRestResponseNotOkResponseNull() {
        VaultHelper helper = spy(new VaultHelper(mockProviderConfig, LOG));
        assertThrows(VaultConfigurationProviderException.class, () -> helper.checkRestResponse(null));
    }

    @Test
    void checkRestResponseNotOkRestResponseNull() {
        VaultHelper helper = spy(new VaultHelper(mockProviderConfig, LOG));
        doReturn(null).when(mockLogicalResponse).getRestResponse();
        assertThrows(VaultConfigurationProviderException.class, () -> helper.checkRestResponse(mockLogicalResponse));
    }

    @Test
    void testConnectionPathSet() {
        VaultHelper helper = spy(new VaultHelper(mockProviderConfig, LOG));
        final String path = "/path";

        JsonObject mockObject = mock(JsonObject.class);
        doReturn(Optional.of(path)).when(mockProviderConfig).getTestPath();
        doReturn(mockLogicalResponse).when(helper).getData(eq(path));
        doReturn(mockObject).when(mockLogicalResponse).getDataObject();
        helper.testConnection();

        verify(helper, times(1)).getData(eq(path));
    }

    @Test
    void testConnectionPathNotSet() {
        VaultHelper helper = spy(new VaultHelper(mockProviderConfig, LOG));
        final String path = "/path";

        JsonObject mockObject = mock(JsonObject.class);
        doReturn(Optional.empty()).when(mockProviderConfig).getTestPath();
        helper.testConnection();

        verify(helper, never()).getData(eq(path));
    }

}
