package io.axual.utilities.config.providers;

/*-
 * ========================LICENSE_START=================================
 * vault-config-provider
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import com.google.common.io.Resources;

import com.bettercloud.vault.Vault;
import com.bettercloud.vault.VaultConfig;
import com.bettercloud.vault.VaultException;
import com.bettercloud.vault.api.Auth;
import com.bettercloud.vault.response.AuthResponse;

import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigException;
import org.apache.kafka.common.config.types.Password;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

import io.axual.utilities.config.providers.exceptions.VaultConfigurationException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class VaultHelperConfigTest {

    public static final String EXPECTED_SECRET_ID = "some-secret-id";
    public static final String EXPECTED_APPROLE_ID = "some-approle-id";
    public static final String EXPECTED_APPROLE_PATH = "/testing/login/approle";
    public static final String EXPECTED_TRUSTSTORE_LOCATION = Resources.getResource("ssl/vault.truststore.jks").getPath();//"Trust is here";
    public static final String EXPECTED_TRUSTSTORE_PASSWORD = "truststorePassword";
    public static final String EXPECTED_PREFIX_PATH = "/one/two/three/four";
    public static final Integer EXPECTED_PREFIX_PATH_DEPTH = 3;
    public static final Integer EXPECTED_GLOBAL_ENGINE_VERSION = -1;
    public static final String EXPECTED_NAMESPACE = "test_ns";
    public static final Integer EXPECTED_READ_TIMEOUT = 12;
    public static final Integer EXPECTED_OPEN_TIMEOUT = 24;
    public static final String EXPECTED_ADDRESS = "http://localvault:1234";
    public static final String EXPECTED_TEST_PATH = "/test/path";

    private static final String EXTENDED_NAME_CONFIG = "keyName";
    private static final String EXPECTED_EXTENDED_NAME = "testName";
    private static final String EXTENDED_BOOLEAN_CONFIG = "booleanName";
    private static final Boolean EXPECTED_EXTENDED_BOOLEAN = Boolean.FALSE;

    private static final String EXTENDED_INT_CONFIG = "intName";
    private static final Integer EXPECTED_EXTENDED_INT = -99;
    private static final ConfigDef EXTENSION_DEF = new ConfigDef()
            .define(EXTENDED_NAME_CONFIG, ConfigDef.Type.STRING, ConfigDef.Importance.HIGH, "Test Config")
            .define(EXTENDED_BOOLEAN_CONFIG, ConfigDef.Type.BOOLEAN, null, ConfigDef.Importance.HIGH, "Test Boolean Config")
            .define(EXTENDED_INT_CONFIG, ConfigDef.Type.INT, null, ConfigDef.Importance.HIGH, "Test Int Config");

    private static final ConfigDef EXTENDED_DEF = VaultHelperConfig.addVaultHelperConfigDefinitions(new ConfigDef(EXTENSION_DEF));

    public static final Map<String, Object> CONFIG_BASE;
    public static final Map<String, Object> CONFIG_APPROLE;
    public static final Map<String, Object> CONFIG_SSL;
    public static final Map<String, Object> CONFIG_EXTENDED;

    static {
        Map<String, Object> config_base = new HashMap<>();
        config_base.put(VaultHelperConfig.VAULT_TEST_PATH_CONFIG, EXPECTED_TEST_PATH);
        config_base.put(VaultHelperConfig.VAULT_ADDRESS_CONFIG, EXPECTED_ADDRESS);
        config_base.put(VaultHelperConfig.VAULT_TIMEOUT_OPEN_CONFIG, EXPECTED_OPEN_TIMEOUT);
        config_base.put(VaultHelperConfig.VAULT_TIMEOUT_READ_CONFIG, EXPECTED_READ_TIMEOUT);
        config_base.put(VaultHelperConfig.VAULT_NAMESPACE_CONFIG, EXPECTED_NAMESPACE);
        config_base.put(VaultHelperConfig.VAULT_GLOBAL_ENGINE_VERSION_CONFIG, EXPECTED_GLOBAL_ENGINE_VERSION);
        config_base.put(VaultHelperConfig.VAULT_PREFIX_PATH_DEPTH_CONFIG, EXPECTED_PREFIX_PATH_DEPTH);
        config_base.put(VaultHelperConfig.VAULT_PREFIX_PATH_CONFIG, EXPECTED_PREFIX_PATH);
        CONFIG_BASE = Collections.unmodifiableMap(config_base);

        Map<String, Object> config_approle = new HashMap<>(CONFIG_BASE);
        config_approle.put(VaultHelperConfig.VAULT_AUTH_METHOD_CONFIG, VaultHelperConfig.VAULT_AUTH_METHOD_APPROLE);
        config_approle.put(VaultHelperConfig.VAULT_CREDENTIAL_APPROLE_PATH_CONFIG, EXPECTED_APPROLE_PATH);
        config_approle.put(VaultHelperConfig.VAULT_CREDENTIAL_APPROLE_ROLE_ID_CONFIG, EXPECTED_APPROLE_ID);
        config_approle.put(VaultHelperConfig.VAULT_CREDENTIAL_APPROLE_SECRET_ID_CONFIG, EXPECTED_SECRET_ID);
        CONFIG_APPROLE = Collections.unmodifiableMap(config_approle);

        Map<String, Object> config_ssl = new HashMap<>(CONFIG_APPROLE);
        config_ssl.put(VaultHelperConfig.VAULT_SSL_TRUSTSTORE_LOCATION_CONFIG, EXPECTED_TRUSTSTORE_LOCATION);
        config_ssl.put(VaultHelperConfig.VAULT_SSL_TRUSTSTORE_PASSWORD_CONFIG, EXPECTED_TRUSTSTORE_PASSWORD);
        CONFIG_SSL = Collections.unmodifiableMap(config_ssl);

        final Map<String, Object> extended = new HashMap<>(CONFIG_SSL);
        extended.put(EXTENDED_NAME_CONFIG, EXPECTED_EXTENDED_NAME);
        extended.put(EXTENDED_BOOLEAN_CONFIG, EXPECTED_EXTENDED_BOOLEAN);
        extended.put(EXTENDED_INT_CONFIG, EXPECTED_EXTENDED_INT);
        CONFIG_EXTENDED = Collections.unmodifiableMap(extended);
    }


    @Mock
    Vault mockedVault;

    @Mock
    Auth mockedAuth;


    @Captor
    ArgumentCaptor<String> appRolePathCaptor;

    @Captor
    ArgumentCaptor<String> appRoleIdCaptor;

    @Captor
    ArgumentCaptor<String> secretIdCaptor;

    @Test
    void checkConstructorsExtendedSuccess() {
        Map<String, Object> props = new HashMap<>(CONFIG_EXTENDED);
        assertDoesNotThrow(() -> new VaultHelperConfig(EXTENDED_DEF, props));
        assertDoesNotThrow(() -> new VaultHelperConfig(EXTENDED_DEF, props, true));
        assertDoesNotThrow(() -> new VaultHelperConfig(props));
        assertDoesNotThrow(() -> new VaultHelperConfig(props, true));
    }

    @Test
    void checkConstructorsExtendedFailOnConfig() {
        Map<String, Object> props = new HashMap<>(CONFIG_EXTENDED);
        props.remove(EXTENDED_NAME_CONFIG);
        assertThrows(ConfigException.class, () -> new VaultHelperConfig(EXTENDED_DEF, props));
        assertThrows(ConfigException.class, () -> new VaultHelperConfig(EXTENDED_DEF, props, true));
    }

    @Test
    void checkConstructorsExtendedFailOnDefinition() {
        Map<String, Object> props = new HashMap<>(CONFIG_EXTENDED);
        ConfigDef INCOMPLETE = new ConfigDef();
        assertThrows(IllegalArgumentException.class, () -> new VaultHelperConfig(INCOMPLETE, props));
        assertThrows(IllegalArgumentException.class, () -> new VaultHelperConfig(INCOMPLETE, props, true));
    }

    @Test
    void checkEquality() {
        Map<String, Object> props = new HashMap<>(CONFIG_BASE);
        VaultHelperConfig config1 = new VaultHelperConfig(props);
        VaultHelperConfig config2 = new VaultHelperConfig(props);

        assertNotNull(config1);
        assertNotNull(config2);
        assertEquals(config1, config2);
        assertEquals(config1.hashCode(), config2.hashCode());
    }

    @Test
    void checkInequality() {
        Map<String, Object> props = new HashMap<>(CONFIG_BASE);
        VaultHelperConfig config1 = new VaultHelperConfig(props);
        props.remove(VaultHelperConfig.VAULT_TEST_PATH_CONFIG);
        VaultHelperConfig config2 = new VaultHelperConfig(props);

        assertNotNull(config1);
        assertNotNull(config2);
        assertNotEquals(config1, config2);
        assertNotEquals(config1.hashCode(), config2.hashCode());
    }

    @Test
    void checkInequalityVaultConfig() {
        Map<String, Object> props = new HashMap<>(CONFIG_BASE);
        VaultHelperConfig config1 = new VaultHelperConfig(props);
        VaultHelperConfig config2 = new VaultHelperConfig(props);
        assertNotNull(config2.getVaultConfig());

        assertNotNull(config1);
        assertNotNull(config2);
        assertNotEquals(config1, config2);
        assertNotEquals(config1.hashCode(), config2.hashCode());
    }

    @Test
    void getVaultConfigSuccessNoSll() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        VaultHelperConfig config = new VaultHelperConfig(props);
        VaultConfig vaultConfig1 = config.getVaultConfig();
        assertVaultConfig(vaultConfig1);
        VaultConfig vaultConfig2 = config.getVaultConfig();
        assertVaultConfig(vaultConfig2);
        assertEquals(vaultConfig1, vaultConfig2);
    }

    @Test
    void getVaultConfigSuccessWithSsl() {
        Map<String, Object> props = new HashMap<>(CONFIG_SSL);
        VaultHelperConfig config = new VaultHelperConfig(props);
        VaultConfig vaultConfig1 = config.getVaultConfig();
        assertVaultConfig(vaultConfig1);
    }

    private void assertVaultConfig(VaultConfig vaultConfig) {
        assertEquals(EXPECTED_ADDRESS, vaultConfig.getAddress());
        assertEquals(EXPECTED_NAMESPACE, vaultConfig.getNameSpace());
        assertNull(vaultConfig.getToken());
        assertEquals(EXPECTED_GLOBAL_ENGINE_VERSION, vaultConfig.getGlobalEngineVersion());

        assertNotNull(vaultConfig.getSslConfig());
    }

    @Test
    void loginAppRoleSuccess() throws VaultException {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        VaultHelperConfig config = new VaultHelperConfig(props);

        AuthResponse mockedResponse = mock(AuthResponse.class);
        when(mockedVault.auth()).thenReturn(mockedAuth);

        when(mockedAuth.loginByAppRole(anyString(), anyString(), anyString())).thenReturn(mockedResponse);

        AuthResponse response = config.login(mockedVault);

        verify(mockedAuth, atLeastOnce()).loginByAppRole(appRolePathCaptor.capture(), appRoleIdCaptor.capture(), secretIdCaptor.capture());

        assertFalse(appRolePathCaptor.getAllValues().isEmpty());
        assertEquals(EXPECTED_APPROLE_PATH, appRolePathCaptor.getAllValues().get(0));

        assertFalse(appRoleIdCaptor.getAllValues().isEmpty());
        assertEquals(EXPECTED_APPROLE_ID, appRoleIdCaptor.getAllValues().get(0));

        assertFalse(secretIdCaptor.getAllValues().isEmpty());
        assertEquals(EXPECTED_SECRET_ID, secretIdCaptor.getAllValues().get(0));

        assertEquals(mockedResponse, response);
    }

    @Test
    void loginAppRoleFailAuthMethod() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        props.put(VaultHelperConfig.VAULT_AUTH_METHOD_CONFIG, "none");
        VaultHelperConfig config = new VaultHelperConfig(props);
        assertThrows(VaultConfigurationException.class, () -> config.login(mockedVault));
    }

    @Test
    void loginAppRoleFailAuthRoleId() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        props.remove(VaultHelperConfig.VAULT_CREDENTIAL_APPROLE_ROLE_ID_CONFIG);
        VaultHelperConfig config = new VaultHelperConfig(props);
        assertThrows(VaultConfigurationException.class, () -> config.login(mockedVault));
    }

    @Test
    void getTestPathSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalSet(EXPECTED_TEST_PATH, config::getTestPath);
    }

    @Test
    void getTestPathNotSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        props.remove(VaultHelperConfig.VAULT_TEST_PATH_CONFIG);
        VaultHelperConfig config = new VaultHelperConfig(props);
        assertOptionalNotSet(config::getTestPath);
    }

    @Test
    void getAddressSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalSet(EXPECTED_ADDRESS, config::getAddress);
    }

    @Test
    void getAddressNotSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        props.remove(VaultHelperConfig.VAULT_ADDRESS_CONFIG);
        try {
            VaultHelperConfig config = new VaultHelperConfig(props);
            fail("Expecting a ConfigException");
        } catch (ConfigException e) {
            assertTrue(e.getMessage().contains(VaultHelperConfig.VAULT_ADDRESS_CONFIG), "No mention of missing address in exeption");
        }
    }

    @Test
    void getOpenTimeoutSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalSet(EXPECTED_OPEN_TIMEOUT, config::getOpenTimeout);
    }

    @Test
    void getOpenTimeoutNotSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        props.remove(VaultHelperConfig.VAULT_TIMEOUT_OPEN_CONFIG);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalSet(VaultHelperConfig.VAULT_TIMEOUT_OPEN_DEFAULT, config::getOpenTimeout);
    }

    @Test
    void getReadTimeoutSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalSet(EXPECTED_READ_TIMEOUT, config::getReadTimeout);
    }

    @Test
    void getReadTimeoutNotSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        props.remove(VaultHelperConfig.VAULT_TIMEOUT_READ_CONFIG);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalSet(VaultHelperConfig.VAULT_TIMEOUT_READ_DEFAULT, config::getReadTimeout);
    }

    @Test
    void getNameSpaceSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalSet(EXPECTED_NAMESPACE, config::getNameSpace);
    }

    @Test
    void getNameSpaceNotSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        props.remove(VaultHelperConfig.VAULT_NAMESPACE_CONFIG);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalNotSet(config::getNameSpace);
    }

    @Test
    void getGlobalEngineVersionSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalSet(EXPECTED_GLOBAL_ENGINE_VERSION, config::getGlobalEngineVersion);
    }

    @Test
    void getGlobalEngineVersionNotSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        props.remove(VaultHelperConfig.VAULT_GLOBAL_ENGINE_VERSION_CONFIG);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalSet(VaultHelperConfig.VAULT_GLOBAL_ENGINE_VERSION_DEFAULT, config::getGlobalEngineVersion);
    }

    @Test
    void getPrefixPathDepthSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalSet(EXPECTED_PREFIX_PATH_DEPTH, config::getPrefixPathDepth);
    }

    @Test
    void getPrefixPathDepthNotSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        props.remove(VaultHelperConfig.VAULT_PREFIX_PATH_DEPTH_CONFIG);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalSet(VaultHelperConfig.VAULT_PREFIX_PATH_DEPTH_DEFAULT, config::getPrefixPathDepth);
    }

    @Test
    void getPrefixPathSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalSet(EXPECTED_PREFIX_PATH, config::getPrefixPath);
    }

    @Test
    void getPrefixPathNotSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        props.remove(VaultHelperConfig.VAULT_PREFIX_PATH_CONFIG);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalNotSet(config::getPrefixPath);
    }

    @Test
    void getSsLVerifySetTrue() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        props.put(VaultHelperConfig.VAULT_SSL_VERIFY_CONFIG, Boolean.TRUE);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalSet(Boolean.TRUE, config::getSslVerify);
    }

    @Test
    void getSsLVerifySetFalse() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        props.put(VaultHelperConfig.VAULT_SSL_VERIFY_CONFIG, Boolean.FALSE);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalSet(Boolean.FALSE, config::getSslVerify);
    }

    @Test
    void getSsLVerifyNotSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        props.remove(VaultHelperConfig.VAULT_SSL_VERIFY_CONFIG);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalSet(VaultHelperConfig.VAULT_SSL_VERIFY_DEFAULT, config::getSslVerify);
    }

    @Test
    void getSslTruststoreLocationSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        props.putAll(CONFIG_SSL);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalSet(EXPECTED_TRUSTSTORE_LOCATION, config::getSslTruststoreLocation);
    }

    @Test
    void getSslTruststoreLocationNotSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalNotSet(config::getSslTruststoreLocation);
    }

    @Test
    void getSslTruststorePasswordNotSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalNotSet(config::getSslTruststorePassword);
    }

    @Test
    void getSslTruststorePasswordSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        props.putAll(CONFIG_SSL);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalSet(new Password(EXPECTED_TRUSTSTORE_PASSWORD), config::getSslTruststorePassword);
    }

    @Test
    void getAuthMethodAppRole() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        props.put(VaultHelperConfig.VAULT_AUTH_METHOD_CONFIG, VaultHelperConfig.VAULT_AUTH_METHOD_APPROLE);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalSet(VaultHelperConfig.VAULT_AUTH_METHOD_APPROLE, config::getAuthMethod);
    }

    @Test
    void getAuthMethodNotSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        props.remove(VaultHelperConfig.VAULT_AUTH_METHOD_CONFIG);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalSet(VaultHelperConfig.VAULT_AUTH_METHOD_DEFAULT, config::getAuthMethod);
    }

    @Test
    void getAppRolePathSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalSet(EXPECTED_APPROLE_PATH, config::getAppRolePath);
    }

    @Test
    void getAppRolePathNotSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        props.remove(VaultHelperConfig.VAULT_CREDENTIAL_APPROLE_PATH_CONFIG);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalSet(VaultHelperConfig.VAULT_CREDENTIAL_APPROLE_PATH_DEFAULT, config::getAppRolePath);
    }

    @Test
    void getAppRoleIdSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalSet(EXPECTED_APPROLE_ID, config::getAppRoleId);
    }

    @Test
    void getAppRoleIdNotSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        props.remove(VaultHelperConfig.VAULT_CREDENTIAL_APPROLE_ROLE_ID_CONFIG);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalNotSet(config::getAppRoleId);
    }

    @Test
    void getAppRoleSecretIdSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalSet(new Password(EXPECTED_SECRET_ID), config::getAppRoleSecretId);
    }

    @Test
    void getAppRoleSecretIdNotSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        props.remove(VaultHelperConfig.VAULT_CREDENTIAL_APPROLE_SECRET_ID_CONFIG);
        VaultHelperConfig config = new VaultHelperConfig(props);

        assertOptionalNotSet(config::getAppRoleSecretId);
    }

    @Test
    void extendConfigDefFailNoValue() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        props.remove(EXTENDED_NAME_CONFIG);
        assertThrows(ConfigException.class, () -> new VaultHelperConfig(EXTENDED_DEF, props));
    }

    @Test
    void extendConfigDefFailIncorrectDef() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        assertThrows(IllegalArgumentException.class, () -> new VaultHelperConfig(EXTENSION_DEF, props));
    }

    @Test
    void extendConfigDefSuccess() {
        Map<String, Object> props = new HashMap<>(CONFIG_APPROLE);
        props.put(EXTENDED_NAME_CONFIG, EXPECTED_EXTENDED_NAME);
        VaultHelperConfig config = new VaultHelperConfig(EXTENDED_DEF, props);

        assertEquals(EXPECTED_EXTENDED_NAME, config.getString(EXTENDED_NAME_CONFIG));
    }

    @Test
    void getOptionalsSet() {
        Map<String, Object> props = new HashMap<>(CONFIG_EXTENDED);
        VaultHelperConfig config = new VaultHelperConfig(EXTENDED_DEF, props);

        assertGetOptionalSet(EXPECTED_APPROLE_ID, config.getOptionalString(VaultHelperConfig.VAULT_CREDENTIAL_APPROLE_ROLE_ID_CONFIG));
        assertGetOptionalSet(EXPECTED_EXTENDED_INT, config.getOptionalInt(EXTENDED_INT_CONFIG));
        assertGetOptionalSet(EXPECTED_EXTENDED_BOOLEAN, config.getOptionalBoolean(EXTENDED_BOOLEAN_CONFIG));
        assertGetOptionalSet(new Password(EXPECTED_SECRET_ID), config.getOptionalPassword(VaultHelperConfig.VAULT_CREDENTIAL_APPROLE_SECRET_ID_CONFIG));
    }

    @Test
    void getOptionalsNotSetValue() {
        Map<String, Object> props = new HashMap<>(CONFIG_EXTENDED);
        props.remove(VaultHelperConfig.VAULT_CREDENTIAL_APPROLE_ROLE_ID_CONFIG);
        props.remove(EXTENDED_INT_CONFIG);
        props.remove(EXTENDED_BOOLEAN_CONFIG);
        props.remove(VaultHelperConfig.VAULT_CREDENTIAL_APPROLE_SECRET_ID_CONFIG);
        VaultHelperConfig config = new VaultHelperConfig(EXTENDED_DEF, props);

        assertGetOptionalNotSet(config.getOptionalString(VaultHelperConfig.VAULT_CREDENTIAL_APPROLE_ROLE_ID_CONFIG));
        assertGetOptionalNotSet(config.getOptionalBoolean(EXTENDED_BOOLEAN_CONFIG));
        assertGetOptionalNotSet(config.getOptionalPassword(VaultHelperConfig.VAULT_CREDENTIAL_APPROLE_SECRET_ID_CONFIG));
        assertGetOptionalNotSet(config.getOptionalInt(EXTENDED_INT_CONFIG));
    }

    @Test
    void getOptionalsNotSetOriginals() {
        Map<String, Object> props = new HashMap<>(CONFIG_EXTENDED);
        VaultHelperConfig config = new VaultHelperConfig(EXTENDED_DEF, props);

        assertGetOptionalNotSet(config.getOptionalString("NotExist"));
        assertGetOptionalNotSet(config.getOptionalBoolean("NotExist"));
        assertGetOptionalNotSet(config.getOptionalPassword("NotExist"));
        assertGetOptionalNotSet(config.getOptionalInt("NotExist"));
    }

    private <T> void assertGetOptionalSet(T expected, Optional<T> get) {
        assertNotNull(get);
        assertTrue(get.isPresent());
        assertEquals(expected, get.get());
    }

    private <T> void assertGetOptionalNotSet(Optional<T> get) {
        assertNotNull(get);
        assertFalse(get.isPresent());
    }


    private <T> void assertOptionalSet(T expected, Supplier<Optional<T>> supplier) {
        Optional<T> value = supplier.get();
        assertNotNull(value);
        assertTrue(value.isPresent());
        assertEquals(expected, value.get());
    }

    private <T> void assertOptionalNotSet(Supplier<Optional<T>> supplier) {
        Optional<T> value = supplier.get();
        assertNotNull(value);
        assertFalse(value.isPresent());
    }
}
