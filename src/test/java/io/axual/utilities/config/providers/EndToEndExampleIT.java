package io.axual.utilities.config.providers;

/*-
 * ========================LICENSE_START=================================
 * Configuration Provider for HashiCorp Vault
 * %%
 * Copyright (C) 2020 - 2024 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.utilities.config.providers.util.KafkaContainer;
import io.axual.utilities.config.providers.util.VaultTestContainer;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.DescribeClusterResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Example of end-to-rnd usage of the vault config provider.
 * This sets up a cluster, stores connection data in vault, then sets up a client using the vault ConfigProvicer.
 */
@Testcontainers
@Slf4j
class EndToEndExampleIT {

    @Container
    KafkaContainer kafka = new KafkaContainer().withAutoCreateTopics(true);

    @Container
    VaultTestContainer vault = VaultTestContainer.getInstance();

    @BeforeEach
    void setup() {
        kafka.start();
        vault.start();
    }

    @Test
    void canConnectToKafkaUsingVault() throws Exception {
        // GIVEN kafka client connection properties stored in vault
        Map<String, Object> connectionProperties = kafka.clientConnectionProperties();
        vault.addResource("resources/kafka/clientProps", connectionProperties);

        // AND the vault configuraiton provider added to the configuration
        Map<String, Object> clientConfig = new HashMap<>();
        clientConfig.put("config.providers", "vault");
        clientConfig.put("config.providers.vault.class", "io.axual.utilities.config.providers.VaultConfigProvider");
        clientConfig.put("config.providers.vault.param.address", vault.getAddress());
        clientConfig.put("config.providers.vault.param.auth.method", "APPROLE");
        clientConfig.put("config.providers.vault.param.approle.role.id", vault.getRoleId());
        clientConfig.put("config.providers.vault.param.approle.secret.id", vault.getSecretId());

        // WHEN we configure a client with properties from vault configprovider
        clientConfig.put("bootstrap.servers", "${vault:resources/kafka/clientProps:bootstrap.servers}");
        clientConfig.put("security.protocol", "${vault:resources/kafka/clientProps:security.protocol}");
        clientConfig.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        clientConfig.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        // THEN decribing the cluster should work without exceptions
        try (AdminClient adminClient = AdminClient.create(clientConfig)) {
            DescribeClusterResult description = adminClient.describeCluster();
            assertNotNull(description);
            String clusterId = description.clusterId().get();
            log.info("Got cluster ID: {}", clusterId);
        }
    }
}
